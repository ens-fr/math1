# Suites géométriques

On a vu que pour une suite arithmétique $(u_n)$, on a pour tout $n\in \mathbb N$ : $u_{n+1} = u_n + r$, où $r$ est la raison.

De la même manière, on dira qu'une suite $(v_n)$ est **géométrique**[^1], si pour tout $n\in \mathbb N$ : $v_{n+1} = v_n \times q$, où $q$ est la raison.

[^1]: :material-wikipedia: [Suite géométrique](https://fr.wikipedia.org/wiki/Suite_g%C3%A9om%C3%A9trique)


!!! question "Pourquoi $q$ ?"
    $q$ comme quotient. Si les termes sont non nuls, on a pour tout $n\in \mathbb N$ : $\dfrac {v_{n+1}}{  v_n } = q$.

!!! question "Pourquoi $v$ ?"
    On met ce qu'on veut. On peut utiliser $u$, si c'est disponible.

!!! question "Y a-t-il une formule pour le terme d'indice $n$ ?"
    Oui. Étudions ça.

On considère une suite géométrique de premier terme $v_0$ et de raison $q$. On a :

* $v_0 = v_0 = v_0\times q^0$
* $v_1 = v_0 \times q = v_0\times q^1$
* $v_2 = v_0 \times q\times q = v_0\times q^2$
* $v_3 = v_0 \times q\times q \times q= v_0\times q^3$

Et de manière générale :

!!! tip "Formule"
    Pour tout $n\in\mathbb N$, on a $v_n = v_0 \times q^n$

!!! example "Exemple avec NumWorks"
    ![def](./images/G-def.png){ .bordure }
    ![graph](./images/G-graph.png){ .bordure }
    ![tab](./images/G-tab.png){ .bordure }

    Pour obtenir la variable `n` sur la calculatrice, on peut appuyer sur la touche ++"x, n, t"++, en haut, à côté de ++"alpha"++.

    Comme pour toute suite numérique, on peut visualiser la suite sur un graphique ainsi que dans un tableau.

## Utilisations classiques

!!! question "Exercice type"
    Une suite géométrique est telle que $u_{10}=14.13$ et $u_{12}= 16.481232$. Quelle est la raison de cette suite ?

!!! success "Réponse"
    La suite est géométrique, elle est donc de la forme $v_n = v_0×q^n$ où $v_0$ et $q$ sont des réels inconnus.

    On sait que

    - $v_{10} = v_0×q^{10} = 14.13$
    - $v_{12} = v_0×q^{12} = 16.481232$

    On déduit $v_{12} = v_0×q^{10}×q^2 = 14.13×q^2 = 16.481232$
    
    Et enfin que $q^2 = \dfrac{16.481232}{14.13} = 1.1664$

    Il y a **deux solutions** :

    - $q = \sqrt{1.1664} = 1.08$
    - ou bien $q = -\sqrt{1.1664} = -1.08$

### Augmentation en pourcentage

!!! note "Rappel"
    Augmenter une quantité de $t\,\%$ revient à la multiplier par $\left(1+\dfrac t{100}\right)$.

    Exemple : $2300$ augmenté de $7\,\%$ est égal à $2300×\left(1+\dfrac 7{100}\right) = 2461$.

Ainsi une situation avec un point de départ donné et une augmentation à **taux constant** est modélisée avec une **suite géométrique**.

:warning: Si l'augmentation est à _valeur constante_, c'est une _suite arithmétique_.

:warning: Pour une diminution, il suffit de considérer une augmentation avec un taux négatif.

### Avec Python

- Un récipient fuit et perd $3\%$ de sa contenance chaque jour.
- Il est rempli de $30\,000\,\text{L}$ au début.

Vous pouvez lancer (et modifier) le script Python ci-dessous :

{{ IDE('fuite') }}

!!! question "Question"
    À quoi sert le script Python ci-dessus ?

!!! success "Réponse"
    Ce script modélise la situation avec une suite géométrique et donne le nombre de jours à attendre pour que le récipient contienne moins de $30\,000\,\text{L}$.

    Au bout de **89 jours**, il y aura moins de $30\,000\,\text{L}$.

## Somme de termes consécutifs d'une suite géométrique

### Avec premier terme égal à 1

On pose $q\neq1$, et on considère :
$S_n = 1+q+q^2+\cdots+q^{n-1}+q^n$

On a $qS_n = q+q^2+q^3\cdots+q^n+q^{n+1}$, on déduit $qS_n - S_n = q^{n+1} - 1$, d'où 

$$S_n = \dfrac{q^{n+1} - 1}{q - 1}$$


!!! tip "si $q \neq 1$"

    $$1+q+q^2+\cdots+q^{n-1}+q^n = \dfrac{q^{n+1} - 1}{q - 1}$$

    :warning: Cette formule est à retenir !

!!! note "si $0 \leqslant q < 1$"
    On peut utiliser une variante équivalente

    $$1+q+q^2+\cdots+q^{n-1}+q^n = \dfrac{1 - q^{n+1}}{1 - q}$$

    L'avantage étant que le numérateur et le dénominateur deviennent positifs.

!!! note "si $q=1$"
    La suite est aussi arithmétique dans ce cas, de raison $1$.

    $1+q+q^2+\cdots+q^{n-1}+q^n = n+1$, si $q=1$.

    **Parfois**, même, on peut lire que si $q=1$ la suite n'est pas géométrique.


### Avec premier terme quelconque

Il suffit de factoriser par le premier terme.

$S_n = v_0 + v_1 + v_2 + \cdots + v_{n-1} + v_n$

$S_n = v_0\times 1+v_0\times q+ v_0\times q^2+\cdots+v_0\times q^{n-1}+v_0\times q^n$

$S_n = v_0\times \left(1+q+q^2+\cdots+q^{n-1}+q^n\right)$

On se ramène alors au cas précédent.

