# Programme du DS

**La calculatrice sera interdite pour ce DS**.

> Les calculs seront tous très simples.

- Savoir factoriser (second degré en particulier).
- Savoir résoudre des inéquations et des équations.
- Savoir dériver une fonction polynôme ; exemple $f(x) = 5x^7 + 3x^2 +9$.
- Savoir dériver un quotient de deux polynômes ; exemple $g(x) = \dfrac{x^3-5x+8}{x^2+7}$.
- Savoir dériver un produit de polynôme par une racine de fonction affine ; exemple $h(x) = \sqrt{8x -1} (4x^3 - 2x^2 + x)$.
- Savoir créer un script Python qui donne l'équation d'une tangente. On supposera que les fonctions `f` et `f_prime` sont disponibles.
- Savoir donner le tableau de variations d'une fonction très simple après l'avoir dérivée ; exemple $k(x) = 5x^3 + 2x^2 - 6x + 3$.