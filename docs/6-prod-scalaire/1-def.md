# Généralités

Pour deux vecteurs $\overrightarrow {AB}\binom xy$ et $\overrightarrow {AC}\binom {x'}{y'}$ le produit scalaire $\overrightarrow {AB} \cdot \overrightarrow {AC}$ indique le défaut d'orthogonalité des deux vecteurs. C'est un nombre (d'où l'adjectif scalaire) qui est nul quand les deux vecteurs sont orthogonaux.

!!! quote "Angle formé par deux vecteurs non nuls"
    On note $\left(\overrightarrow {AB}\,, \overrightarrow {AC}\right)$ l'angle orienté formé par deux vecteurs non nuls.

    :warning: Si l'un des vecteurs est nul, l'angle n'est pas défini.

    $\cos\left(\overrightarrow {AB}\,, \overrightarrow {AC}\right)$ désigne donc le cosinus de l'angle est insensible à l'orientation.

    $$\cos\left(\overrightarrow {AB}\,, \overrightarrow {AC}\right) = \cos\left(\overrightarrow {AC}\,, \overrightarrow {AB}\right)$$

## Façons de calculer

### Avec l'angle

Si les deux vecteurs sont non nuls, alors :

$\overrightarrow {AB} \cdot \overrightarrow {AC} = AB × AC × \cos\left(\overrightarrow {AB}\,, \overrightarrow {AC}\right)$

Si un des vecteurs est nul, le cosinus n'est pas défini, mais on acceptera de dire que c'est un nombre inconnu de $-1$ à $+1$, qui multiplié par $0$, donne $0$.

On retrouve donc 

- $\overrightarrow {AB} \cdot \vec 0 = 0$
- $\vec 0 \cdot \overrightarrow {AC} = 0$

### Avec le projeté orthogonal

Si $\overrightarrow {AB}$ est non nul, la droite $(AB)$ existe, on considère $H$ le projeté orthogonal de $C$ sur $(AB)$, on a alors :

$\overrightarrow {AB} \cdot \overrightarrow {AC} = \overrightarrow {AB} \cdot \overrightarrow {AH}$

Dans ce cas, et si $\overrightarrow {AH}$ est non nul, on a $\cos\left(\overrightarrow {AB}\,, \overrightarrow {AH}\right) = \pm 1$, de sorte que $\overrightarrow {AB} \cdot \overrightarrow {AC} = \pm AB×AH$, suivant le sens des deux vecteurs.

### De manière analytique

$\overrightarrow {AB} \cdot \overrightarrow {AC} = xx' + yy'$

Cette formule est toujours valable.

### Avec des distances

$\overrightarrow {AB} \cdot \overrightarrow {AC} = \frac12 (AB^2 + AC^2 - BC^2)$

Cette formule traduit bien le défaut d'orthogonalité.

## Propriétés

### Propriétés algébriques

Pour tous vecteurs $\vec u$, $\vec v$, $\vec w$, et tout réel $k$, on a :

1. $\vec u \cdot \vec 0 = 0$
1. $\vec 0 \cdot \vec v = 0$
1. $\vec u \cdot \vec v = \vec v \cdot \vec u$, le produit scalaire est commutatif
1. $\vec u \cdot (\vec v + \vec w) = \vec u \cdot \vec v + \vec u \cdot \vec w$
1. $(\vec u + \vec v) \cdot \vec w = \vec u \cdot \vec w + \vec v \cdot \vec w$
1. $\vec u \cdot (k \vec v) = (k\vec u) \cdot \vec v = k(\vec u \cdot \vec v)$


!!! info "Rappel"
    Si $\vec u = \binom xy$ et $k\in\Bbb R$, alors $k\vec u = \binom{kx}{ky}$

### Formules des normes

Pour tous vecteurs $\vec u$, $\vec v$, on a :

1. $\vec u \cdot \vec v = \frac12\left(\lVert \vec u + \vec v\rVert^2 - \lVert \vec u \rVert^2 - \lVert \vec v\rVert^2 \right)$
1. $\vec u \cdot \vec v = \frac12\left(\lVert \vec u\rVert^2 + \lVert \vec v \rVert^2 - \lVert  \vec u - \vec v\rVert^2 \right)$
1. $\vec u \cdot \vec v = \frac14\left(\lVert \vec u + \vec v \rVert^2 - \lVert  \vec u - \vec v\rVert^2 \right)$


!!! tip "Astuce"
    Il est souvent plus facile de retrouver les formules suivantes :

    $\lVert \vec u + \vec v\rVert^2 = \lVert \vec u \rVert^2 + 2\vec u \cdot \vec v + \lVert \vec v\rVert^2$

    $\lVert \vec u - \vec v\rVert^2 = \lVert \vec u \rVert^2 - 2\vec u \cdot \vec v + \lVert \vec v\rVert^2$

### Orthogonalité

!!! quote "Définition"
    On dit que deux vecteurs $\vec u$ et $\vec v$ sont orthogonaux quand $\vec u \cdot \vec v = 0$.

!!! warning "Vecteurs non nuls"
    Pour deux vecteurs $\overrightarrow {AB}$ et $\overrightarrow {CD}$ non nuls, alors les droites $(AB)$ et $(CD)$ existent et 

    $(AB)$ et $(CD)$ sont perpendiculaires si el seulement si $\overrightarrow {AB} \cdot \overrightarrow {CD} = 0$

## Équation de droite

L'équation d'une droite qui n'est pas verticale peut être donnée sous la forme $\mathcal D: y = mx+p$, ce qui s'écrit aussi $\mathcal D: mx +(-1)×y +(-p) = 0$

L'équation d'une droite verticale peut être donnée par $x = q$, ce qui s'écrit aussi $\mathcal D: 1×x + 0×y +(-q) = 0$

Ainsi, dans tous les cas une équation de droite peut s'écrire $\mathcal D:ax+by+c = 0$.

Réciproquement, toute équation de la forme $D: ax+by+c=0$ avec $\binom ab \neq \vec 0$ est une équation d'une droite.

1. Si $b\neq 0$, on a $\mathcal D: y = \frac{-a}b x + \frac{-c}b$, une droite non verticale ;
2. Si $b = 0$, on a $\mathcal D: x = \frac{-c}a$


!!! info "Propriété"
    Pour $a, b, c\in \Bbb R$, avec $a$ et $b$ **non** tous deux nuls,

    1. $ax+by+c=0$ est l'équation d'une droite qui admet $\vec n\binom ab$ comme vecteur normal.
    2. Si $\vec n\binom ab$ est un vecteur normal à une droite $\mathcal D$, alors il existe $c$ telle que une équation cartésienne de $\mathcal D$ soit $ax+by+c=0$.

!!! quote "Vecteur directeur"
    Si $\vec n \binom ab$ est un vecteur normal à la droite $\mathcal D$, alors le vecteur $\binom{-b}a$ est un vecteur directeur de $\mathcal D$ ; et réciproquement.