
# Degré 3

Les équations de degré 3, de manière générale, ne sont pas au programme. Cependant, les cas particuliers se ramenant au degré 2 peuvent donner lieu à des exercices.

!!! info "Propriétés : identifier les coefficients"
    Dans $\mathbb R$, si deux polynômes sont égaux, alors ils ont les mêmes coefficients.

    Exemple : Si $2x^3 -7x^2+2x+3 = ax^3 +bx^2+cx+d$, alors $a=2$, $b=-7$, $c=2$ et $d=3$.

    :warning: Cela semble évident, mais cette propriété est fausse si on ne travaille pas dans $\mathbb R$.


!!! example "Exemple de factorisation"
    $f(x) = 2x^3 -7x^2+2x+3$

    $f(x)$ est un polynôme de degré 3 en $x$.

    Dans ce cas particulier, on peut vérifier que $f(1) = 2-7+2+3 = 0$, ainsi $1$ est une racine évidente de $f(x) = 0$. On peut alors factoriser $f(x)$ par $(x-1)$ et obtenir un autre facteur de degré 2.

    $(x-1)(ax^2 + bx + c) = 2x^3 -7x^2+2x+3$, où $a$, $b$ et $c$ sont à déterminer. Pour les déterminer, on développe le membre de gauche et on identifie les coefficients.

    - $f(x) = (x-1)(ax^2 + bx + c)$
    - $f(x) = (ax^3 + bx^2 + cx) - (ax^2 + bx + c)$
    - $f(x) = ax^3 + (b-a)x^2 + (c-b)x - c$
    - $f(x) = 2x^3 -7x^2+2x+3$

    On déduit immédiatement $a=2$ et $c=-3$.

    Il reste les deux informations

    - $b - a = -7$, d'où $b = -7 + 2 = -5$
    - $c - b = 2$, pour vérifier, $-3 - (-5) = -3 + 5 = 2$. OK.

    Ainsi $f(x) = (x-1)(2x^2-5x-3)$

    Pour factoriser $f(x)$ entièrement, il suffit alors de factoriser un trinôme du second degré.
    

    !!! tip "Conseils"
        Face à un polynôme de degré supérieur à deux,
        
        - on utilisera la calculatrice pour déterminer des racines et, si possible, leur valeur exacte.
        - On vérifiera ensuite, par le calcul, les conjectures.
        - On factorisera par $(x-r)$ pour chaque racine $r$ déterminée.

        S'il le facteur restant est toujours de degré supérieur à deux, on peut essayer un changement de variable...
