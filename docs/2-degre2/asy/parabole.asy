import graph;

size(10cm,0);

real xmin=-3,xmax=3;

xaxis(Label("$x$",align=Align),xmax=3.4,Ticks(NoZero),Arrow);
yaxis(Ticks(NoZero));
label("O", (0, 0), SW);

// Définition de la fonction
real f(real x) {return -0.2*x^2 -2*x/3 -3;}
real g(real x) {return          -2*x/3 -3;}


// Tracé de la courbe
path Cf=graph(f,xmin,xmax,n=400);
draw(Cf,linewidth(2bp));
path Cg=graph(g,xmin,xmax,n=400);
draw(Cg,dashed+linewidth(1.5bp));
label("$\mathcal{C}_f$",(2.5, f(2.5)), NE);

shipout(bbox(5mm));
