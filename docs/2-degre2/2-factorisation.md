# Factorisation

Après avoir identifié un trinôme du second degré, on calcule son discriminant et alors, trois cas se présentent.

- le discriminant est strictement négatif
- le discriminant est nul
- le discriminant est strictement positif


## Les trois cas du discriminant

### Discriminant strictement négatif

Si $\Delta < 0$

- L'équation $f(x) = 0$ n'a pas de solution réelle.
- Factorisation : On ne peut pas factoriser $f(x)$ dans les réels.
- Le signe de $f(x)$ est constant, celui de $a$.

![](./asy/par2.svg){ .autolight }
![](./asy/par3.svg){ .autolight }

!!! tip "Preuve"
    Avec $f(x) = a\left[\left(x - \dfrac{-b}{2a}  \right)^2  - \dfrac \Delta {4a^2} \right]$ et $\Delta < 0$, on observe que $f(x)$ est le produit de $a$ par la somme d'un carré et d'un nombre strictement positif, donc ne s'annule pas et reste du signe de $a$.


### Discriminant nul

- L'équation $f(x) = 0$ a une unique solution réelle $x = \dfrac{-b}{2a}$
- Factorisation : $f(x) = a\left(x - \dfrac{-b}{2a}  \right)^2$
- Le signe de $f(x)$ est constant, celui de $a$, sauf en $x = \dfrac{-b}{2a}$

!!! tip "Preuve"
    Avec $f(x) = a\left[\left(x - \dfrac{-b}{2a}  \right)^2  - \dfrac \Delta {4a^2} \right]$ et $\Delta = 0$, on tire :

    $f(x) = a\left(x - \dfrac{-b}{2a}  \right)^2$

### Discriminant strictement positif

- L'équation $f(x) = 0$ a deux solutions réelles $\dfrac{-b-\sqrt \Delta}{2a}$ et $\dfrac{-b+\sqrt \Delta}{2a}$, qu'on appelle les racines.
- Factorisation : $f(x) = a\left(x - \dfrac{-b-\sqrt \Delta}{2a}  \right)\left(x - \dfrac{-b+\sqrt \Delta}{2a}  \right)$
- Le signe de $f(x)$ change, c'est
    - celui de $a$ en dehors des racines
    - nul aux racines
    - l'opposé de celui de $a$ entre les racines

!!! example "Exemple de résolution"

    $$\mathcal E_0 : 3x^2 + 8x + 5 = 0$$

    On pose $f(x) = 3x^2 + 8x + 5$, c'est un trinôme du second degré en $x$, dont le discriminant est $\Delta = 8^2 - 4×3×5 = 64 - 60 = 4$.

    On constate que $\Delta > 0$, avec $\sqrt\Delta = \sqrt 4 = 2$, ainsi on peut dire que ses racines sont :

    $$x_1 = \dfrac{-8 - 2}{2×3} \text{ et } x_2 = \dfrac{-8 + 2}{2×3}$$

    Les solutions de l'équation sont $-1$ et $\frac{-5}3$.

    On peut factoriser $f(x) = 3\left(x-(-1)\right)\left(x-\frac{-5}3\right)$, d'où

    $f(x) = (x+1)(3x+5)$

    !!! note "Vérification"
        On développe à nouveau pour vérifier

        - $f(x) = (x+1)(3x+5)$
        - $f(x) = x×3x +x×5 +1×3x +1×5$
        - $f(x) = 3x^2 +5x +3x +5$
        - $f(x) = 3x^2 +8x +5$ ; OK

## Fonctions Python

On peut écrire des fonctions Python utiles que vous pouvez tester.

{{ IDE('racines') }}

Lancer le script, puis testez

```pycon
>>> %Script exécuté
>>> racines(3, 8, 5)
[-1.6666666666666667, -1.0]
```

On reconnait bien $\dfrac{-5}3$ et $-1$.
