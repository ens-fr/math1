# Rappels du collège

!!! info "Cours du collège et de seconde"
    Commençons par quelques rappels utiles : la trigonométrie pour des angles aigus, en degré, dans des triangles rectangles.

    ![](./images/geometrygeeks-bike.png)

    > Source[^1]
    [^1]: Source pour l'image des côtes de vélo : <https://geometrygeeks.bike/>


    Ensuite, pour le cours de première, on généralisera la notion à tous les angles, on utilisera une autre unité d'angle (le radian) et on étudiera les fonctions sinus, cosinus et tangente.

## Le triangle rectangle

!!! tip "Propriété 1"
    Un triangle rectangle possède un angle droit, qui mesure $90°$, et deux angles aigus complémentaires.

    L'hypoténuse, le côté en face de l'angle droit, est le plus grand des côtés.

    ??? note "Preuve"
        ![](./asy/t1.svg){.autolight align="right"}

        Dans le triangle $ABC$, la somme des mesures des angles est égale à $180°$, ainsi

        $\widehat{CAB} + \widehat{ABC} + \widehat{BCA} = 180°$, avec $\widehat{BCA}=90°$, $\alpha>0$, $\beta>0$, on tire

        $\alpha+\beta = 90°$, $\alpha<90°$ et $\beta<90°$

        :white_check_mark: $\alpha$ et $\beta$ sont donc aigus et complémentaires.

        Dans le triangle $ABC$, rectangle en $C$, le théorème de Pythagore nous assure que $AB^2 = AC^2+CB^2$, ainsi

        $AB^2 > AC^2$, avec $AB$ et $AC$ positifs, on tire que $AB > BC$. De même, $AB > AC$.

        :white_check_mark: $[AB]$ est donc le plus grand des côtés.


!!! tip "Définition"
    ![](./asy/t2.svg){.autolight align="left"}

    Dans un triangle rectangle $ABC$, rectangle en $C$,
    pour un angle aigu $\widehat{CAB}$, on définit :

    - le côté **O**pposé à $\widehat{CAB}$ est $[BC]$
    - l'**H**ypoténuse est $[AB]$
    - le côté **A**djacent à $\widehat{CAB}$ est $[AC]$

    :octicons-light-bulb-16: Adjacent signifie « à côté de »


    - $\sin\left(\widehat{CAB}\right) = \dfrac{BC}{AB} = \dfrac{\text{Opposé}}{\text{Hypoténuse}}$
    - $\cos\left(\widehat{CAB}\right) = \dfrac{AC}{AB} = \dfrac{\text{Adjacent}}{\text{Hypoténuse}}$
    - $\tan\left(\widehat{CAB}\right) = \dfrac{BC}{AC} = \dfrac{\text{Opposé}}{\text{Adjacent}}$

    Cette définition est invariante par translation, rotation et symétrie du triangle.

    !!! note "Moyen mnémotechnique"
        Une fois l'angle aigu choisi, on note au brouillon
        
        - `H`, l'**H**ypoténuse,
        - `O`, le côté **O**pposé à l'angle aigu
        - `A`, le côté **A**djacent (à côté de)
        - `S`, `C` et `T` désigneront **S**inus, **C**osinus et **T**angente.

        On utilise le code `SOH CAH TOA` à retenir et on tire :

        - `SOH` : **S**inus de l'angle aigu, égal à côté **O**posé sur **H**ypoténuse.
        - `CAH` : **C**osinus de l'angle aigu, égal à côté **A**djacent sur **H**ypoténuse.
        - `TOA` : **T**angente de l'angle aigu, égal à côté **O**posé sur côté **A**djacent.
        

!!! tip "Propriété 2"
    Les définitions précédentes ne dépendent pas du triangle rectangle choisi, mais uniquement de la mesure de l'angle.

    !!! note "Preuve"
        ![](./asy/t3.svg){.autolight align="right"}
        On considère deux triangles rectangles ayant un angle de mesure commune.

        On peut déplacer, tourner, renverser l'un des triangles de manière à partager l'angle commun et avoir les côtés opposés à l'angle qui soient parallèles. On obtient une configuration de Thalès.

        En appliquant le théorème de Thalès dans cette configuration, on tire l'égalité des lignes trigonométriques : elles ne dépendent pas du triangle, mais uniquement de l'angle !
    
    ??? note "Détail"
        $(BC)$ et $(B'C')$ sont toutes deux perpendiculaires à la même droite, ainsi $(BC)$ et $(B'C')$ sont parallèles. D'après le théorème de Thalès, on a un tableau de proportionnalité

        |$AB$|$BC$|$CA$|
        |----|----|----|
        |$AB'$|$B'C'$|$C'A$|

        On en déduit que les tableaux suivants sont proportionnels

        |$BC$|$B'C'$|
        |----|----|
        |$AB$|$AB'$|

        |$CA$|$C'A$|
        |----|----|
        |$AB$|$AB'$|

        |$BC$|$B'C'$|
        |----|----|
        |$CA$|$C'A$|

        Et donc que

        - Le sinus de $\widehat{BAC}$ est le même dans les deux triangles.
        - Le cosinus de $\widehat{BAC}$ est le même dans les deux triangles.
        - La tangente de $\widehat{BAC}$ est le même dans les deux triangles.
        

!!! tip "Propriété 3"
    Pour un angle aigu, $0 < x < 90°$, on a :

    - $0 < \sin(x) < 1$
    - $0 < \cos(x) < 1$
    - $0 < \tan(x)$

    :warning: ce sera différent, si l'angle $x$ n'est pas aigu !
    
    !!! note "Preuve"
        C'est immédiat avec la définition où les longueurs sont strictement positives et l'hypoténuse le plus grand des côtés.


!!! tip "Propriété 4"
    Pour un angle aigu $0 < x < 90°$,

    $\tan(x) = \dfrac{\sin(x)}{\cos(x)}$

    !!! note "Preuve"
        Avec $\sin(x)=\dfrac{BC}{AB}$, $\cos(x) = \dfrac{AC}{AB}$ et $\tan (x) = \dfrac{BC}{AC}$, on a

        $\dfrac{\sin(x)}{\cos(x)} = \dfrac{BC}{AB} ÷ \dfrac{AC}{AB}$

        $\dfrac{\sin(x)}{\cos(x)} = \dfrac{BC}{AB} × \dfrac{AB}{AC}$

        $\dfrac{\sin(x)}{\cos(x)} = \dfrac{BC×AB}{AB×AC}$

        $\dfrac{\sin(x)}{\cos(x)} = \dfrac{BC}{AC} = \tan(x)$






## Fonctions trigonométriques

Le fait que le sinus, cosinus et tangente ne dépendent que de l'angle et pas du triangle rectangle choisi justifie que la calculatrice puisse donner des valeurs sans connaitre le triangle. On suppose qu'on travaille en degré dans cette partie.

Il existe donc des fonctions :

- $\sin$ : renvoie le sinus de la mesure d'angle donnée.
- $\cos$ : renvoie le cosinus de la mesure d'angle donnée.
- $\tan$ : renvoie la tangente de la mesure d'angle donnée.

Ces fonctions renvoient des nombres sans unités.

Il existe aussi les fonctions réciproques :

- $\arcsin$ : renvoie l'arc sinus du nombre donné.
- $\arccos$ : renvoie l'arc cosinus du nombre donné.
- $\arctan$ : renvoie l'arc tangente du nombre donné.

!!! tip "Propriété"
    Ces fonctions renvoient un angle tel que :

    Si $\alpha$ est un angle aigu, alors 

    - $\arcsin(\sin(\alpha)) = \alpha$
    - $\arccos(\cos(\alpha)) = \alpha$
    - $\arctan(\tan(\alpha)) = \alpha$

    Si $0 < x < 1$, alors

    - $\sin(\arcsin(x)) = x$
    - $\cos(\arccos(x)) = x$

    Si $0 < x$, alors

    - $\tan(\arctan(x)) = x$

!!! warning "Attention à la suite"
    Bientôt, on travaillera avec des angles quelconques et ces égalités ne seront plus valables.

    Il faut retenir que pour les angles aigus, c'est plus facile.

> On commence la leçon avec l'unité d'angle en degré. Il faut vérifier et savoir changer d'unité.

!!! cite "Notations variées"
    Les fonctions réciproques sont notées différemment suivant les pays et les usages.

    On trouve $\text{arcsin}$, $\text{asin}$, $\text{sin}^{-1}$ pour l'arc sinus, et de même pour les autres.

### NumWorks

On vérifie que `deg` soit écrit en haut à gauche de l'écran. Sinon, on va dans les paramètres pour le modifier.

![](./images/deg1.png){ .bordure }
![](./images/deg2.png){ .bordure }
![](./images/deg3.png){ .bordure }

On peut alors effectuer des calculs

![](./images/calcul.png){ .bordure }
![](./images/cos.png){ .bordure }

- Les fonctions $\sin$, $\cos$ et $\tan$ sont directement accessibles.

![](./images/sincostan.png){width="200"}

- Les fonctions réciproques sont accessibles après appui sur ++"shift"++

![](./images/shift.png){width="120"}



### Python

Python travaille par défaut en radian (c'est plus scientifique), et non en degré. Pour obtenir les mêmes résultats que précédemment, on l'utilise ainsi :

```pycon
>>> from math import *  # (1)!
>>> cos(radians(22))     # (2)!
0.9271838545667874
>>> degrees(acos(0.34))    # (3)!
70.12312592992117
>>>
```

1. C'est mal de faire ça, il vaut mieux éviter...
2. L'angle de $22°$ est converti en radian et passé en paramètre à la fonction `cos`.
3. `#!py acos(0.34)` est un angle en radian ; on veut ici le convertir en degré...

!!! warning "Numworks et Python"
    Quand on utilise Python dans Numworks, le mode degré n'est pas pris en compte dans les paramètres, il est uniquement valable dans la partie **calculs**. Python dans NumWorks reste Python ! Et c'est très bien !

    :warning: Avec Python, on préfèrera travailler avec des radians ! Et ce sera plus simple.


### GeoGebra

- GeoGebra affiche les angles en degré,
- mais effectue tous les calculs en radian, quitte à faire une conversion.
- On peut aussi indiquer qu'un angle est en degré.

!!! example "Expérience 1"
    1. On crée un polygone régulier à trois côtés.
    2. On construit l'angle $\widehat{BAC}$
    3. Dans le champ de saisie : `cos(α)` ; le caractère `α` est accessible dans le menu d'édition, de même que la fonction `cos`.

    ![](./images/geogebra.png)

    - On obtient bien `0.5`
    - On peut aussi entrer `cos(60°)` :warning: ne pas oublier d'indiquer l'unité en degré, sinon le calcul est effectué en radian !

!!! example "Expérience 2"
    - On crée un curseur variable, un angle $\alpha$ de 0° à 90°
    - On crée un point $M = (\cos(\alpha), \sin(\alpha))$
    - On partage l'**appliquette dynamique**, voici le résultat :

    <iframe scrolling="no" title="Point mobile" src="https://www.geogebra.org/material/iframe/id/cpwhzsga/width/403/height/621/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="403px" height="621px" style="border:0px;"> </iframe>

    - Cliquez sur les boutons :octicons-play-16: ou :fontawesome-regular-circle-pause:.
    - Déplacez le curseur de l'angle.
    - Conjecturez la nature de la courbe obtenue par l'ensemble des points $M$ !
    - Prouvez votre conjecture !


## Exercices élémentaires

- Si on sait qu'un triangle est rectangle, et si on connait une mesure d'angle aigu, on peut déterminer l'autre facilement, ils sont complémentaires !
- Si on sait qu'un triangle est rectangle, et si on a **deux** informations parmi les longueurs de côté et un angle aigu, alors on peut déterminer les autres longueurs et angles.

### Exercice 1

![](./asy/ex1.svg){ .autolight }

$ABC$ est un triangle rectangle en $A$, tel que $\widehat{ABC} = 22°$ et $AB = 13\,\text{cm}$. Déterminer $BC$.

??? success "Réponse"
    !!! tip "Au brouillon uniquement"
        :warning: Ne pas écrire ceci sur une copie.

        Dans le triangle rectangle $ABC$, avec l'angle aigu $\widehat{ABC}$,

        - $AB$ est le côté **A**djacent.
        - $BC$ est l'**H**ypoténuse.
        - (<code><s>SOH</s> <strong>CAH</strong> <s>TOA</s></code>)

        On va donc utiliser le **C**osinus.

    $ABC$ est un triangle rectangle en $A$, en considérant l'angle aigu $\widehat{ABC}$, on a :

    - $\cos\left(\widehat{ABC}\right) = \dfrac{AB}{BC}$
    - $\dfrac{\cos(22°)}1 = \dfrac{13}{BC}$
    - $\boxed{BC = \dfrac{13}{\cos(22°)}\approx 14,\!02095\,\text{cm}}$

    !!! tip "Explications"
        1. On cite le triangle en question, le sommet de l'angle droit et l'angle aigu choisi. Il pourra y avoir plusieurs triangles dans d'autres exercices...
        2. On réfléchit **au brouillon** sur la fonction trigonométrique à choisir.
        3. On écrit la formule de manière littérale.
        4. On fait les substitutions utiles.
        5. Si nécessaire, on écrit un quotient avec un dénominateur 1.
        6. On peut alors, ici, calculer comme une quatrième proportionnelle.
        7. On donne la valeur exacte **et** approchée, avec la bonne unité.
        8. :warning: Vérifier, sur le schéma que le résultat trouvé est plausible !
            - Ici, oui, $[BC]$ est un peu plus grand que $[AB]$.

### Exercice 2

![](./asy/ex2.svg){ .autolight }

$RST$ est un triangle rectangle en $R$, avec $RT=13\,\text{km}$ et $RS = 21\,\text{km}$. Déterminer $\widehat{RST}$

??? success "Réponse"
    !!! tip "Au brouillon uniquement"
        :warning: Ne pas écrire ceci sur une copie.

        Dans le triangle rectangle $RST$, avec l'angle aigu $\widehat{RST}$,

        - $RS$ est le côté **A**djacent.
        - $RT$ est le côté **O**pposé.
        - (<code><s>SOH</s> <s>CAH</s> <strong>TOA</strong></code>)

        On va donc utiliser la **T**angente.


    $RST$ est un triangle rectangle en $R$, en considérant l'angle aigu $\widehat{RST}$, on a :

    - $\tan\left(\widehat{RST}\right) = \dfrac{RT}{RS}$
    - $\tan\left(\widehat{RST}\right) = \dfrac{13}{21}$, avec $\widehat{RST}$ un angle aigu, ainsi
    - $\boxed{\widehat{RST} = \arctan\left(\dfrac{13}{21}\right)\approx 31,\!76°}$


### Exercice 3

![](./asy/ex3.svg){ .autolight align="left"}

On considère la figure suivante avec :

- $MAV$ est un triangle rectangle en $M$, avec $VM=3\,\text{cm}$ et $\widehat{MVA} = 57^\circ$.
- $DAV$ est un triangle rectangle en $A$, avec $AD=2\,\text{cm}$.
- Déterminer $\widehat{VDA}$

??? success "Réponse"
    !!! tip "Au brouillon uniquement"
        :warning: Ne pas écrire ceci sur une copie.

        1. On va d'abord calculer $V\!A$, puis $\widehat{ADV}$.
        2. Dans le triangle $M\!AV$, avec l'angle aigu $\widehat{MV\!A}$,
            - $MV$ est le côté **A**djacent.
            - $V\!A$ est l'**H**ypoténuse.
            - (<code><s>SOH</s> <strong>CAH</strong> <s>TOA</s></code>)
            - On va donc utiliser le **C**osinus,
        3. Dans le triangle $DAV$, avec l'angle aigu $\widehat{VDA}$,
            - $DA$ est le côté **A**djacent.
            - $VA$ est le côté **O**pposé.
            - (<code><s>SOH</s> <s>CAH</s> <strong>TOA</strong></code>)
            - On va donc utiliser la **T**angente.
    
    $MV\!A$ est un triangle rectangle en $M$, en considérant l'angle aigu $\widehat{MV\!A}$, on a :

    - $\cos\left(\widehat{MV\!A}\right) = \dfrac{MV}{V\!A}$
    - $\dfrac{\cos(57°)}1 = \dfrac{3}{V\!A}$
    - $V\!A = \dfrac{1×3}{\cos(57°)}$

    $V\!AD$ est un triangle rectangle en $A$, en considérant l'angle aigu $\widehat{VDA}$, on a :

    - $\tan\left(\widehat{VDA}\right) = \dfrac{V\!A}{AD}$
    - $\tan\left(\widehat{VDA}\right) = \dfrac{3}{\cos(57°)}÷2$, avec $\widehat{VDA}$ un angle aigu,
    - $\boxed{\widehat{VDA} = \arctan\left(\dfrac{3}{2\cos(57°)}\right)\approx 70°}$

    



### Autres exercices

Utilisez <https://enligne.pyromaths.org/>, onglet Troisième, section Trigonométrie.

Créez plusieurs exercices, vous obtiendrez un fichier `pdf` avec le corrigé à part.

Vous pouvez ainsi réviser et vous autoévaluer jusqu'à ce que le concept soit acquis de manière solide.

## Préparation de la suite du cours

On souhaite calculer les valeurs exactes de certaines lignes trigonométriques. On cherche alors des configurations avec un triangle rectangle et des longueurs connues ou calculées.

??? question "Quelle configuration avec un angle de 45° ?"
    Dans un triangle rectangle isocèle !

??? question "Quelle configuration avec un angle de 60° ?"
    Dans un triangle équilatéral partagé en deux suivant un axe de symétrie !

??? question "Quelle configuration avec un angle de 30° ?"
    Dans un triangle équilatéral partagé en deux suivant un axe de symétrie !


