# Formules d'additions

## Exercices

:warning: Vous ne devez pas inventer de formule !

!!! question "Exercice 1 : $\cos(a+b) =\;?$"
    Que pensez-vous de la formule : $\cos(a+b) = \cos(a) + \cos(b)$ pour $a, b \in \mathbb R$ ?
    
    On testera avec, successivement :
    
    1. $a = b = 0$
    2. $a = b = \dfrac{\pi}2$
    3. $a = b = \pi$
    4. $a = b = \dfrac{\pi}4$

??? success "Réponse"
    1. $\cos(0+0) = \cos(0) = 1$, or $\cos(0) + \cos(0) = 1 + 1 = 2$. La formule est donc **fausse** !
    2. $\cos\left(\dfrac{\pi}2+\dfrac{\pi}2\right) = \cos(\pi) = -1$, or $\cos\left(\dfrac{\pi}2\right) + \cos\left(\dfrac{\pi}2\right) = 0 + 0 = 0$. Confirmation, la formule est donc **fausse** !
    3. $\cos(\pi+\pi) = \cos(2\pi) = 1$, or $\cos(\pi) + \cos(\pi) = -1 + (-1) = -2$. La formule est donc **fausse** !
    4. $\cos\left(\dfrac{\pi}4+\dfrac{\pi}4\right) = \cos\left(\dfrac{\pi}2\right) = 0$, or $\cos\left(\dfrac{\pi}4\right) + \cos\left(\dfrac{\pi}4\right) = \dfrac{\sqrt2}2 +\dfrac{\sqrt2}2 = \sqrt2$. Confirmation, la formule est donc **fausse** !

    Un seul des quatre cas suffit à prouver que la formule est fausse !

    :warning: Avant d'utiliser une formule qu'on a appris, il est très facile de la tester rapidement pour se conforter ou bien la corriger !

!!! tip "Formule [1] hors programme"
    La connaissance de la formule n'est plus au programme, en revanche, son utilisation peut faire l'objet d'exercices. On donnera une démonstration en fin de page de cette formule.

    Pour $a, b \in \mathbb R$

    $$\cos(a-b) = \cos(a)\cos(b) + \sin(a)\sin(b)$$

!!! example "Tests"
    On essayera avec, successivement :
    
    1. $a = b = 0$
    2. $a = b = \dfrac{\pi}2$
    3. $a = b = \pi$
    4. $a = b = \dfrac{\pi}4$

    Ce n'est pas une preuve, mais juste une vérification qu'il faut savoir faire.

??? success "Réponse"
    Dans les quatre cas, on a $a=b$, donc $\cos(a-b) = \cos(0) = 1$

    1.  Avec $a = b = 0$,
        - $t_1 = \cos(a)\cos(b) + \sin(a)\sin(b)$
        - $t_1 = \cos(0)\cos(0) + \sin(0)\sin(0)$
        - $t_1 = 1×1 + 0×0$
        - $t_1 = 1+0$
        - $t_1 = 1$
    2.  Avec $a = b = \dfrac{\pi}2$,
        - $t_2 = \cos(a)\cos(b) + \sin(a)\sin(b)$
        - $t_2 = \cos\left(\dfrac{\pi}2\right)\cos\left(\dfrac{\pi}2\right) + \sin\left(\dfrac{\pi}2\right)\sin\left(\dfrac{\pi}2\right)$
        - $t_2 = 0×0 + 1×1$
        - $t_2 = 0+1$
        - $t_2 = 1$
    3.  Avec $a = b = \pi$
        - $t_3 = \cos(a)\cos(b) + \sin(a)\sin(b)$
        - $t_3 = \cos(\pi)\cos(\pi) + \sin(\pi)\sin(\pi)$
        - $t_3 = (-1)×(-1) + 0×0$
        - $t_3 = 1 + 0$
        - $t_3 = 1$
    4.  Avec $a = b = \dfrac{\pi}4$
        - $t_4 = \cos(a)\cos(b) + \sin(a)\sin(b)$
        - $t_4 = \cos\left(\dfrac{\pi}4\right)\cos\left(\dfrac{\pi}4\right) + \sin\left(\dfrac{\pi}4\right)\sin\left(\dfrac{\pi}4\right)$
        - $t_4 = \left(\dfrac{\sqrt2}2\right)^2 +\left(\dfrac{\sqrt2}2\right)^2$
        - $t_4 = \frac{2}4 + \frac{2}4$
        - $t_4 = \frac{4}4$
        - $t_4 = 1$


!!! question "Exercice 2"
    En utilisant la formule [1], donner une formule pour $\cos(a+b)$.

    !!! tip "Indices"
        1. On pourra écrire $\cos(a+b) = \cos(a-(-b))$, utiliser la formule [1], puis les formules $\cos(-x) = \cos(x)$ et $\sin(-x) = -\sin(x)$.
        2. On pourra tester le résultat avec quelques valeurs !

    ??? success "Réponse"
        Pour $a, b\in \mathbb R$, on a

        - $E_2 = \cos(a+b)$
        - $E_2 = \cos(a-(-b))$
        - $E_2 = \cos(a)\cos(-b) + \sin(a)\sin(-b)$
        - $E_2 = \cos(a)\cos(b) - \sin(a)\sin(b)$

        En conclusion, formule [2] : pour $a, b\in\mathbb R$, on a

        $$\cos(a+b) = \cos(a)\cos(b) - \sin(a)\sin(b)$$

!!! question "Exercice 3"
    En utilisant la formule [2], donner une formule pour $\sin(a-b)$.

    !!! tip "Indices"
        1. On pourra utiliser la formule $\sin(x) = \cos\left(\frac{\pi}2 - x\right)$
        2. On pourra alors écrire $\sin(a-b) = \cos\left(\frac{\pi}2 - (a-b)\right)$, puis utiliser la formule [2]
        3. On pourra aussi utiliser la formule $\cos(x) = \sin\left(\frac{\pi}2 - x\right)$
        4. On pourra tester le résultat avec quelques valeurs !

    ??? success "Réponse"
        Pour $a, b\in \mathbb R$, on a

        - $E_3 = \sin(a-b)$
        - $E_3 = \cos\left(\frac{\pi}2 - (a-b)\right)$
        - $E_3 = \cos\left(\left(\frac{\pi}2 - a\right) + b\right)$
        - $E_3 = \cos\left(\frac{\pi}2 - a\right)\cos\left(b\right) - \sin\left(\frac{\pi}2 - a\right)\sin(b)$
        - $E_3 = \sin(a)\cos(b) - \cos(a)\sin(b)$

        En conclusion, formule [3] : pour $a, b\in\mathbb R$, on a

        $$\sin(a-b) = \sin(a)\cos(b) - \cos(a)\sin(b)$$

!!! question "Exercice 4"
    En utilisant la formule [3], donner une formule pour $\sin(a+b)$.

    ??? tip "Indice"
        On pourra écrire $\sin(a+b) = \sin(a-(-b))$

    ??? success "Réponse"
        Pour $a, b\in \mathbb R$, on a

        - $E_4 = \sin(a+b)$
        - $E_4 = \sin(a-(-b))$
        - $E_4 = \sin(a)\cos(-b) - \cos(a)\sin(-b)$
        - $E_4 = \sin(a)\cos(b) + \cos(a)\sin(b)$

        En conclusion, formule [4] : pour $a, b\in\mathbb R$, on a

        $$\sin(a+b) = \sin(a)\cos(b) + \cos(a)\sin(b)$$

!!! question "Exercice 5"
    Lorsque chacun existe, exprimer $\tan(a-b)$ en fonction de $\tan(a)$ et $\tan(b)$.

    ??? success "Réponse"
        - $E_5 = \tan(a-b)$, si $\tan(a-b)$ existe
        - $E_5 = \dfrac{\sin(a-b)}{\cos(a-b)}$
        - $E_5 = \dfrac{\sin(a)\cos(b) - \cos(a)\sin(b)}{\cos(a)\cos(b) + \sin(a)\sin(b)}$ ; divisons par $\cos(a)\cos(b)$ au numérateur et au dénominateur (si $\tan(a)$ et $\tan(b)$ existent)
        - $E_5 = \dfrac{\tan(a) - \tan(b)}{1 + \tan(a)\tan(b)}$

        En conclusion, formule [5] : pour $a, b\in\mathbb R$, si les termes existent, on a

        $$\tan(a-b) = \dfrac{\tan(a) - \tan(b)}{1 + \tan(a)\tan(b)}$$

!!! question "Exercice 6"
    Lorsque chacun existe, exprimer $\tan(a+b)$ en fonction de $\tan(a)$ et $\tan(b)$.

    ??? success "Réponse"
        - $E_6 = \tan(a+b)$, si $\tan(a+b)$ existe
        - $E_6 = \tan(a-(-b))$
        - $E_6 = \dfrac{\tan(a) - \tan(-b)}{1 + \tan(a)\tan(-b)}$
        - $E_6 = \dfrac{\tan(a) + \tan(b)}{1 - \tan(a)\tan(b)}$

        En conclusion, formule [6] : pour $a, b\in\mathbb R$, si les termes existent, on a

        $$\tan(a+b) = \dfrac{\tan(a) + \tan(b)}{1 - \tan(a)\tan(b)}$$


!!! note "Rappel"
    Si $M(x_M ; y_M)$ et $M(x_N ; y_N)$ sont deux points du plan, alors

    $$MN^2 = (x_M - x_N)^2 + (y_M - y_N)^2$$

!!! danger "Démonstration de la formule [1]"
    :warning: La démonstration de cette formule n'est plus au programme.

    On considère $a, b\in\mathbb R$ et $A, B$ leur point image sur le cercle trigonométrique.

    On considère $D$ le point image de l'angle nul, et $E$ le point image de l'angle $a-b$.

    Le triangle $OAB$ est l'image du triangle $OED$ par la rotation de centre $O$ et d'angle $b$.

    ![](./asy/preuve.svg){ .autolight }

    Une rotation conserve les longueurs, ainsi $AB = ED$ que l'on va calculer indépendamment.

    Commençons par $AB^2$.

    - Les coordonnées de $A$ sont $(\cos(a) ; \sin(a))$, et
    - les coordonnées de $B$ sont $(\cos(b) ; \sin(b))$, ainsi
    - $AB^2 = (\cos(a) - \cos(b))^2 + (\sin(a) - \sin(b))^2$, et en développant,
    - $AB^2 = \cos^2(a) - 2\cos(a)\cos(b)+\cos^2(b) + \sin^2(a) - 2\sin(a)\sin(b) +\sin^2(b)$
    - $AB^2 = \cos^2(a)+ \sin^2(a)+\cos^2(b)+\sin^2(b) - 2\cos(a)\cos(b)  - 2\sin(a)\sin(b)$
    - $AB^2 = 2 - 2(\cos(a)\cos(b) + \sin(a)\sin(b))$

    Ensuite $ED^2$.

    - Les coordonnées de $E$ sont $(\cos(a-b) ; \sin(a-b))$, et
    - les coordonnées de $D$ sont $(1 ; 0)$, ainsi

    - $ED^2 = (\cos(a-b) - 1)^2 + (\sin(a-b) - 0)^2$, et en développant,
    - $ED^2 = \cos^2(a-b) -2\cos(a-b) + 1 + \sin^2(a-b)$
    - $ED^2 = 1 + \cos^2(a-b) + \sin^2(a-b) -2\cos(a-b)$
    - $ED^2 = 2 -2\cos(a-b)$

    On déduit alors

    $$\cos(a-b) = \cos(a)\cos(b) + \sin(a)\sin(b)$$

    La formule [1] est démontrée.


## Bilan

:warning: Ce bilan n'est pas à connaitre. Mais on peut faire des exercices qui utilisent ces formules.

Pour $a, b\in\mathbb R$, on a :

- $\sin(a-b) = \sin(a)\cos(b) - \cos(a)\sin(b)$
- $\sin(a+b) = \sin(a)\cos(b) + \cos(a)\sin(b)$
- $\cos(a-b) = \cos(a)\cos(b) + \sin(a)\sin(b)$
- $\cos(a+b) = \cos(a)\cos(b) - \sin(a)\sin(b)$
- $\tan(a-b) = \dfrac{\tan(a) - \tan(b)}{1 + \tan(a)\tan(b)}$, si les termes existent
- $\tan(a+b) = \dfrac{\tan(a) + \tan(b)}{1 - \tan(a)\tan(b)}$, si les termes existent

## Duplication

En utilisant les formules ci-dessus, prouver que si $\tan(a)$ et $\tan(2a)$ existent, alors

- $\sin(2a) = \dfrac{2\tan(a)}{1+\tan^2(a)}$
- $\cos(2a) = \dfrac{1-\tan^2(a)}{1+\tan^2(a)}$
- $\tan(2a) = \dfrac{2\tan(a)}{1-\tan^2(a)}$

!!! tip "Indices"
    - On pourra d'abord prouver que $\dfrac{1}{\cos^2(a)} = 1+\tan^2(a)$, s'il existe.
    - On rappelle que $\cos^2(x) + \sin^2(x) = 1$ pour tout $x\in\mathbb R$.

??? success "Preuve"
    $\dfrac{1}{\cos^2(a)} = \dfrac{\cos^2(a) + \sin^2(a)}{\cos^2(a)} = 1+\tan^2(a)$

    1. $\sin(2a) = \sin(a)\cos(a) + \cos(a)\sin(a) = 2\sin(a)\cos(a) = 2\tan(a)\cos^2(a) = \dfrac{2\tan(a)}{1+\tan^2(a)}$
    2. $\cos(2a) = \cos(a)\cos(a) - \sin(a)\sin(a) = \cos^2(a) - \sin^2(a) = \cos^2(a)(1-\tan^2(a)) = \dfrac{1-\tan^2(a)}{1+\tan^2(a)}$
    3. $\tan(2a) = \dfrac{\tan(a) + \tan(a)}{1 - \tan(a)\tan(a)} = \dfrac{2\tan(a)}{1-\tan^2(a)}$
    