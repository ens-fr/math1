# Lignes et formules classiques

Dans cette page on revoit la preuve de certaines formules de trigonométrie vues au collège et en seconde.

## Angle de 45°

![](./asy/demi-carre.svg){ .autolight align="left"}

On considère le triangle $ABC$ rectangle isocèle de côté commun $1$ ci-contre, d'après le théorème de Pythagore, on a :

$BC^2 = AB^2 + AC^2$

$BC^2 = 1^2+1^2= 1+1 = 2$, avec $BC$ positif, d'où $\boxed{BC = \sqrt2}$

On déduit

$\sin\left(\widehat{ABC}\right) = \dfrac{AC}{BC}$, d'où $\sin(45°) = \dfrac1{\sqrt2}$

$\cos\left(\widehat{ABC}\right) = \dfrac{AB}{BC}$, d'où $\cos(45°) = \dfrac1{\sqrt2}$

$\tan\left(\widehat{ABC}\right) = \dfrac{AC}{AB}$, d'où $\tan(45°) = \dfrac11 = 1$


!!! note "Dénominateur sans radicande"
    $\sin(45°) = \cos(45°) = \dfrac1{\sqrt2} = \dfrac{\sqrt2}{\sqrt2^2} = \dfrac{\sqrt2}2$

    Et de manière plus générale, on peut faire, sans y être obligé, si $a^2\neq b$ :

    $$\dfrac{x}{a+\sqrt b} = \dfrac{x\left(a-\sqrt b\right)}{\left(a+\sqrt b\right)\left(a-\sqrt b\right)} = \dfrac{x\left(a-\sqrt b\right)}{a^2-b}$$
    


## Angles de 30° et 60°

On considère un triangle équilatéral $ABC$ de côté $2$, et $H$ le pied de la hauteur issue de $A$. $AHC$ est un triangle rectangle avec $HC=1$ et $AC=2$, d'après le théorème de Pythagore, on a

$AC^2=AH^2+HC^2$

$2^2=AH^2+1^2$

$4=AH^2+1$

$AH^2=4-1=3$, avec $AH$ positif, d'où $\boxed{AH=\sqrt3}$

![](./asy/equi.svg){ .autolight }

D'autre part, chaque angle du triangle équilatéral mesure $60°$, et $\widehat{HAC}$ en fait la moitié par symétrie.

On déduit

$\sin\left(\widehat{ACH}\right) = \dfrac{AH}{AC}$, d'où $\sin(60°) = \dfrac{\sqrt3}{2}$

$\cos\left(\widehat{ACH}\right) = \dfrac{HC}{AC}$, d'où $\cos(60°) = \dfrac{1}{2}$

$\tan\left(\widehat{ACH}\right) = \dfrac{AH}{HC}$, d'où $\tan(60°) = \dfrac{\sqrt3}{1} = \sqrt3$


$\sin\left(\widehat{HAC}\right) = \dfrac{HC}{AC}$, d'où $\sin(30°) = \dfrac{1}{2}$

$\cos\left(\widehat{HAC}\right) = \dfrac{AH}{AC}$, d'où $\cos(30°) = \dfrac{\sqrt3}{2}$

$\tan\left(\widehat{HAC}\right) = \dfrac{HC}{AH}$, d'où 
$\tan(30°) = \dfrac1{\sqrt3} = \dfrac{\sqrt3}{\sqrt3^2}= \dfrac{\sqrt3}{3}$


## Bilan à connaitre

|Angle|Sinus|Cosinus|Tangente|
|:---:|:---:|:-----:|:------:|
|$30°$|$\dfrac{1}{2}$|$\dfrac{\sqrt3}{2}$|$\dfrac1{\sqrt3}$|
|$45°$|$\dfrac1{\sqrt2}$|$\dfrac1{\sqrt2}$|$1$|
|$60°$|$\dfrac{\sqrt3}{2}$|$\dfrac{1}{2}$|$\sqrt3$|

!!! danger "Confusion acceptable"
    Dans ce tableau on désigne par « angle » la « mesure de l'angle ». La confusion est souvent faite, et acceptable, mais ce sont deux concepts différents. Un angle est un objet géométrique, qui possède une mesure qui est un nombre.

## Angles complémentaires

!!! note "Un constat"
    Dans ce tableau, on constate que 

    - $\sin(30°) = \cos(60°)$
    - $\sin(60°) = \cos(30°)$
    - $\tan(30°)$ et $\tan(60°)$ sont inverses l'un de l'autre.

    Cela se généralise aux angles aigus complémentaires.

Si deux angles aigus sont complémentaires, alors :

- le sinus de l'un est égal au cosinus de l'autre ;
- la tangente de l'un est l'inverse de la tangente de l'autre.

!!! tip "Formules ?"
    Nous écrirons des formules plus générales dans la suite du chapitre.

    On considèrera différentes symétries autour du cercle trigonométrique et des angles (non nécessairement aigus).

## Théorème de Pythagore

Pour tout triangle $ABC$ rectangle en $A$, d'après le théorème de Pythagore, on a :

$AB^2+AC^2=BC^2$, d'où $\dfrac{AB^2}{BC^2}+\dfrac{AC^2}{BC^2}=\dfrac{BC^2}{BC^2}$

On déduit $\left(\dfrac{AB}{BC}\right)^2+\left(\dfrac{AC}{BC}\right)^2=1$ et enfin, en notant $x=\widehat{ABC}$ :

$$\boxed{\cos^2(x) + \sin^2(x) = 1}$$

!!! info "Généralisable"
    Cette formule restera valable pour tout angle, mais il faudra faire attention à un point.
    
    Pour un angle aigu, son sinus et son cosinus sont des nombres positifs, ce ne sera pas nécessairement le cas avec d'autres angles !

!!! example "Utilisation"
    Si $x$ est un angle aigu tel que $\sin(x) = 0,\!3$, alors on a

    $\cos^2(x) + 0,\!3^2 = 1$

    $\cos^2(x) + 0,\!09 = 1$

    $\cos^2(x) =  1 - 0,\!09 = 0,\!91$, :warning: avec $\cos(x)$ positif :warning:, car $x$ est un angle aigu

    $\boxed{\cos(x) = \sqrt{0,\!91} \approx 0,\!953939}$

!!! tip "Quart de cercle trigonométrique"
    ![](./images/geogebra2.png)

    Dans la page précédente, on a conjecturé que les points $M$ de coordonnées $(\cos(\alpha), \sin(\alpha))$ pour tous les angles aigus $\alpha$ forment un quart de cercle de centre $O$ et de rayon $1$. La formule $\cos^2(x) + \sin^2(x) = 1$ permet de prouver cette conjecture.

    Dans la section cercle trigonométrique, on redéfinira les fonctions $\sin$, $\cos$ et $\tan$ pour tous les angles, ce qui donnera un point mobile qui décrira tout le cercle de rayon $1$, en faisant même plusieurs tours !

    