import geometry;
defaultpen(fontsize(14pt)+black); // Stylo par défaut
size(6cm);
triangle t = triangleabc(3, 4, 5, angle=20);
triangle t2 = triangleabc(4.2, 5.6, 7, angle=20);

show(La="", Lb="", Lc="", t, 1bp+black);

show(LA="",LB="$B'$",LC="$C'$",
La="", Lb="", Lc="", t2, 1bp+black);

markangle("$\alpha$", 1, radius=1cm, t.B, t.A, t.C);
//markangle("$\beta$", 2, radius=.5cm, t.C, t.B, t.A);
perpendicularmark(line(t.C,t.B),line(t.A,t.C), quarter=0);
perpendicularmark(line(t2.C,t2.B),line(t2.A,t2.C), quarter=0);
