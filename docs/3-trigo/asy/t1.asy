import geometry;
defaultpen(fontsize(14pt)+black); // Stylo par défaut
size(6cm);
triangle t = triangleabc(3, 4, 5, angle=20);

show(La="", Lb="", Lc="", t, 1bp+black);
markangle("$\alpha$", 1, radius=.5cm, t.B, t.A, t.C);
markangle("$\beta$", 2, radius=.5cm, t.C, t.B, t.A);
perpendicularmark(line(t.C,t.B),line(t.A,t.C), quarter=0);
