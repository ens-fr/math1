import geometry;
defaultpen(fontsize(14pt)+black); // Stylo par défaut

size(3cm);

triangle t2=triangleAbc(90, 10, 10, angle=0);
show(
     La="",Lb="1",Lc="1",
     t2,1bp+black);
          
perpendicularmark(line(t2.A,t2.B),line(t2.A,t2.C), quarter=1);
