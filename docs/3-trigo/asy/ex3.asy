import geometry;
defaultpen(fontsize(14pt)+black); // Stylo par défaut

size(7cm);

triangle t2=triangleAbc(90, 3, 3*Tan(57), angle=-10);
show(LA="$M$",LB="$A$",LC="$V$",
     La="",Lb="$3\,\textrm{cm}$",Lc="",
     t2,1bp+black);
markangle("$57^\circ$",1,radius=0.75cm,
          t2.A,t2.C,t2.B);
          
perpendicularmark(line(t2.A,t2.B),line(t2.A,t2.C), quarter=1);

pair imV=rotate(-90,t2.B)*t2.C;

pair D = t2.B + (imV - t2.B)/3*Cos(57)*2;

label("$D$", D, E);

perpendicularmark(line(t2.C,t2.B),line(t2.B, D), quarter=2);

draw(t2.B--D--t2.C, 1bp+black);

label("$2\,\textrm{cm}$", (t2.B + D)/2, SE);
markangle("?",2,radius=0.75cm,
          t2.C,D,t2.B);
