size(10cm,0);
defaultpen(fontsize(14pt)+black); // Stylo par défaut

import geometry;

pair O=(0,0), pI=(1,0), pJ=(0,1);
dot("$O$", O, SW);
draw(unitcircle);

real angle1=30, angle2=angle1+180;

pair pM=dir(angle1), pN=dir(angle2);
draw(-pJ--pJ);
draw(-pI--pI);

draw(O--pM);
draw("$x$",arc(pI,O,pM,0.35),blue,Arrow);
draw(O--pN);
draw("$x+\pi$",arc(pJ,O,pN,0.25),blue,Arrow);
draw("",arc(pI,O,pJ,0.25),blue);

pair pcos1=(Cos(angle1),0), psin1=(0,Sin(angle1));
draw(pM--pcos1^^pM--psin1,blue+dashed);
perpendicular(pcos1, NW, blue);
perpendicular(psin1, SE, blue);

pair pcos2=(Cos(angle2),0), psin2=(0,Sin(angle2));
draw(pN--pcos2^^pN--psin2,blue+dashed);
perpendicular(pcos2, SE, blue);
perpendicular(psin2, NW, blue);

dot("$M(x)$", pM, dir(O--pM));
dot("$\cos(x)$", pcos1, S+W/2);
dot("$\sin(x)$", psin1, W);

dot("$M'(x+\pi)$", pN, dir(O--pN));
dot("$\cos(x+\pi)$", pcos2, N+W/2);
dot("$\sin(x+\pi)$", psin2, E);

dot("$0$", pI, E, blue);
dot("$\frac{\pi}2$", pJ, N, blue);
dot("$\pi$", -pI, W, blue);
dot("$\frac{3\pi}2$", -pJ, S, blue);
