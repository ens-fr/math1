size(10cm,0);
defaultpen(fontsize(14pt)+black); // Stylo par défaut

import geometry;

pair O=(0,0), pI=(1,0), pJ=(0,1);
dot("$O$", O, SW);
draw(unitcircle);
draw(-1.1*pJ--1.1*pJ, Arrow);
draw(-1.1*pI--1.1*pI, Arrow);
label("$x$", pI, SE);
label("$y$", pJ, NE);

int n=7;
real angle1=180/n;

draw(-pJ--pJ);
draw(-pI--pI);
pair pcos1=(Cos(angle1),0), pF=(1, Tan(angle1)), pE=(1, -Tan(angle1)), pC=(Cos(angle1), -Sin(angle1)), pD=(Cos(angle1), Sin(angle1));
draw(pE--O--pF);
draw("$\frac{\pi}n$",arc(pI,O,pD,0.35),blue,Arrow);

perpendicular(pcos1,NW,blue);
perpendicular((1, 0),NW,red);


dot("$D$", pD, 2*W);
dot("$C$", pC, 2*W);
dot("$E$", pE, E+SE);
dot("$F$", pF, E+NE);
draw(rotate(90)*polygon(n), blue); 
draw(scale(1/cos(pi/n))*rotate(90)*polygon(n), red); 
