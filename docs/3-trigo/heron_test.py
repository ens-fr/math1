# tests
assert racine(42, 2, 0) == 6
assert racine(17**83, 4*17**41, 0) == 1157791181040734142569769628592819189361054248906582

# autres tests

assert racine(111, 80, 0) == 10
assert racine(25, 2, 0) == 5
