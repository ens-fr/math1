def racine(n, depart, marge):
    " Algorithme de Héron d'Alexandrie "
    a = 0
    b = ...  # À compléter
    while abs(b - a) > marge:
        c = ...  # À compléter
        a = b
        b = c
    return b

# tests
assert racine(42, 2, 0) == 6
assert racine(17**83, 4*17**41, 0) == 1157791181040734142569769628592819189361054248906582
