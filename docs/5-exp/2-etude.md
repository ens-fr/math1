# Étude de la fonction

## Signe

!!! quote "Signe de l'exponentielle"
    Pour tout $x\in\mathbb R$, $\mathrm e^x>0$

!!! note "Nouvelle démonstration"
    On a déjà proposé une démonstration, en voici une nouvelle.

    Soit $x\in\mathbb R$, on a

    $$\mathrm e^x = \mathrm e^{\frac x 2 + \frac x 2} = (\mathrm e^{\frac x 2})×(\mathrm e^{\frac x 2}) = (\mathrm e^{\frac x 2})^2 >0$$

    On rappelle que la fonction exponentielle ne s'annule pas sur $\mathbb R$.

!!! example "Exemples indépendants"
    - L'équation $\mathrm e^x = -5$ n'admet aucune solution réelle.
    - $\mathrm e^{-7} > 0$
    - Pour tout $x\in\mathbb R$, $\mathrm e^{-x}>0$
    - Pour tout $x\in\mathbb R$, $-\mathrm e^{3x-4}<0$

## Variation

!!! quote "Propriété"
    La fonction exponentielle est strictement croissante sur $\mathbb R$.

    ---

    Autrement dit : si $x, y \in\mathbb R$ avec $x < y$, alors $\mathrm e^x < \mathrm e^y$.

    ---

    ![tableau de variation](./tabvar-exp.svg){ .autolight }

!!! note "Preuve"
    $\mathrm {exp}'=\mathrm {exp}$, or la fonction exponentielle est strictement positive sur $\mathbb R$, on en déduit qu'elle est strictement croissante sur $\mathbb R$.

!!! quote "Propriété"
    Pour tous réels fixés $k, m, p$, on peut définir $f$ sur $\mathbb R$, avec $f(x) = k×\mathrm e^{mx+p}$.

    $f$ est dérivable sur $\mathbb R$, et on a $f'=mf$.

!!! note "Preuve"
    $f$ est dérivable sur $\mathbb R$ comme composée d'une fonction dérivable sur $\mathbb R$ et d'une fonction affine.

    Pour tout $x\in\mathbb R$, on a $f'(x) = k×m×\mathrm e^{mx+p} = m×f(x)$

!!! example "Exemple"
    On pose $h(x) = -3\mathrm e^{5x+7}$.

    $h$ est dérivable sur $\mathbb R$, et on a

    Pour tout $x\in\mathbb R$, $h'(x) = -3×5×\mathrm e^{5x+7} < 0$

    On déduit que $h$ est décroissante sur $\mathbb R$.

## Courbe représentative

![courbe de l'exponentielle](./graph-exp.svg){ .autolight }

On constate que la courbe représentative est située au-dessus de la tangente au point d'abscisse $0$.

## Quelques valeurs

| $x$  | $\mathrm e^x$|
| ---: | -----------: |
| $-100$ | $3.72007\cdots×10^{-44}$ |
| $-10$ | $4.5399929\cdots×10^{-5}$ |
| $-1$ | $0.367879\cdots$ |
| $0$ | $1$ |
| $+1$ | $2.71828\cdots$ |
| $+10$ | $22~026.46579\cdots$ |
| $+100$ | $2.6881\cdots×10^{43}$ |

Pour une calculatrice classique

- $\mathrm e^{100}$ provoque une erreur de dépassement de capacité
- $\mathrm e^{-100}$ provoque un arrondi à zéro, ce qui est faux en théorie.


## Résolution d'(in)équations

!!! quote "Propriétés"
    Pour tous nombres réels $a$ et $b$ :

    - $\mathrm e^a = \mathrm e^b$ équivaut à $a = b$
    - $\mathrm e^a < \mathrm e^b$ équivaut à $a < b$
    - $\mathrm e^a > \mathrm e^b$ équivaut à $a > b$

    La preuve vient du fait que la fonction exponentielle est strictement croissante.

!!! example "Exemples de résolution"
    ??? question "1. Résoudre $\mathrm e^{2x} = \frac 1e$ sur $\mathbb R$"
        Sur $\mathbb R$, $\mathrm e^{2x} = \frac 1e$ équivaut successivement à

        - $\mathrm e^{2x} = \mathrm e^{-1}$
        - $2x = -1$
        - $\boxed{x = \frac{-1}2}$

    ??? question "2. Résoudre $\mathrm e^{-3x+4} + 1 \geqslant 2$ sur $\mathbb R$"
        Sur $\mathbb R$, $\mathrm e^{-3x+4} + 1 \geqslant 2$ équivaut successivement à

        - $\mathrm e^{-3x+4} \geqslant 1$ ; on a enlevé $1$ dans chaque membre
        - $\mathrm e^{-3x+4} \geqslant \mathrm e^0$ ; on a remplacé $1$ par $\mathrm e^0$
        - $-3x+4 \geqslant 0$ ; on a utilisé la propriété ci-dessus
        - $-3x \geqslant -4$ ; on a enlevé $4$ dans chaque membre
        - $x \leqslant \frac{-4}{-3}$ ; on doit changer le sens de l'inégalité en divisant tout par un nombre négatif
        - $\boxed{x \leqslant \frac{4}{3}}$ ; résultat simplifié.
