# Questions de type QCM

Retrouvez les [questions au format pdf](./1-QCM.pdf)

!!! question "Question 1"
    Quelle est la forme factorisée de $(7y+3)^2-25$ ?

    - [ ] $(49y-22)(49y+28)$
    - [ ] $(7y-22)(7y+28)$
    - [ ] $49y^2+42y-16$
    - [ ] $(7y-2)(7y+8)$

    ??? success "Réponse"
        On constate que c'est une différence entre deux carrés, on peut alors factoriser avec

        $$a^2-b^2 = (a+b)(a-b)$$

        Ainsi, on a successivement :

        - $E = (7y+3)^2-25$
        - $E = (7y+3)^2-5^2$
        - $E = (7y+3+5)(7y+3-5)$
        - $E = (7y+8)(7y-2)$

        !!! tip "Variante"
            On peut aussi procéder rapidement par élimination.

            C'est classique, pour $y=0$, l'expression vaut $9-25=-16$, ce qui élimine deux propositions.

            Parmi les deux restantes, une seule est sous forme factorisée ; on peut donc répondre en quelques secondes à la question.

!!! question "Question 2"
    Quelle est la forme factorisée de $(2x+3)^2-(2x+3)(x-5)$ ?

    - [ ] $(2x+3)(-x-3)$
    - [ ] $(2x+3)(-x+7)$
    - [ ] $(2x+3)(x-2)$
    - [ ] $(2x+3)(x+8)$

    ??? success "Réponse"
        On reconnait une expression de la forme $a^2 -ab$ que l'on peut factoriser par $a$ en $a(a-b)$

        Ainsi, on a successivement :

        - $E = (2x+3)^2-(2x+3)(x-5)$
        - $E = (2x+3)((2x+3) - (x-5))$
        - $E = (2x+3)(2x+3 - x+5)$
        - $E = (2x+3)(x+8)$

        !!! tip "Variante"
            On peut aussi procéder rapidement par élimination.

            C'est classique, pour $x=0$, l'expression vaut $9-(-15)=24$, ce qui élimine trois propositions immédiatement. En effet, pour $x=0$

            - :negative_squared_cross_mark: $(2x+3)(-x-3) = -9$
            - :negative_squared_cross_mark: $(2x+3)(-x+7) = 21$
            - :negative_squared_cross_mark: $(2x+3)(x-2) = -6$
            - :white_check_mark: $(2x+3)(x+8) = 24$

            On peut donc répondre en quelques secondes à la question.



!!! question "Question 3"
    Quelle est la forme développée de $(3x-2)^2 - (5x+2)^2$ ?

    - [ ] $-2x^2+4$
    - [ ] $-16x^2+8x+8$
    - [ ] $-2x^2-4$
    - [ ] $-16x^2-32x$

    ??? success "Réponse"
        :warning: On ne demande pas de factoriser l'expression...

        On a successivement :

        - $E = (3x-2)^2 - (5x+2)^2$
        - $E = (9x^2 - 12x +4) - (25x^2+20x+4)$
        - $E = 9x^2 - 12x +4 - 25x^2-20x-4$
        - $E = -16x^2 - 32x$

        !!! tip "Variante"
            On peut aussi procéder rapidement par élimination.

            C'est classique, pour $x=0$, l'expression vaut $4-4=0$, ce qui élimine trois propositions immédiatement. En effet, pour $x=0$

            - :negative_squared_cross_mark: $-2x^2+4 = 4$
            - :negative_squared_cross_mark: $-16x^2+8x+8 = 8$
            - :negative_squared_cross_mark: $-2x^2-4 = -4$
            - :white_check_mark: $-16x^2-32x = 0$

            On peut donc répondre en quelques secondes à la question.



!!! question "Question 4"
    $(u_n)$ est une suite arithmétique de nombres réels de premier terme $u_0 = 3$ et de raison $2$.
    
    Quelle est la phrase vraie dans la liste ?


    - [ ] On est certain que $u_{5} = 96$
    - [ ] On est certain que $u_n = u_{n+1} + 2$ pour tout $n$
    - [ ] Il est possible que $u_n = u_{n+1} + 2$ pour un certain $n$
    - [ ] Il est impossible d'avoir $u_n = 2022$ pour un certain $n$
    
    ??? success "Réponse"
        La suite est arithmétique de premier terme $u_0 = 3$ et de raison $2$, donc $u_n=2n+3$ et $u_{n+1} = u_n + 2$ pour tout $n\in\mathbb N$.

        1. :negative_squared_cross_mark: $u_5 = 2×5+3 = 13 \neq 96$
        2. :negative_squared_cross_mark: Erreur de signe, ce serait $-2$
        3. :negative_squared_cross_mark: Pour aucun $n$
        4. :white_check_mark: L'équation $2n+3 = 2022$ aurait pour solution $n=1009.5$ qui n'est pas entier. Ainsi $u_n=2022$ est impossible, pour $n\in\mathbb N$.



!!! question "Question 5"
    $(u_n)$ est une suite arithmétique de nombres réels de raison $7$.
  
    Quelle est la phrase vraie dans la liste ?


    - [ ] On est certain que $u_{1000} > 0$
    - [ ] On est certain que $u_{10} > u_{1000}$
    - [ ] Il est possible que $u_{10} > u_{1000}$
    - [ ] Il est possible d'avoir $u_{10} = 2023$

    ??? success "Réponse"
        La suite est arithmétique de raison $7$, donc $u_n=7n + u_0$ pour tout $n\in\mathbb N$.

        1. :negative_squared_cross_mark: Faux, si $u_0 = -8000$, par exemple
        2. :negative_squared_cross_mark: Faux, si $u_0 = 0$, par exemple
        3. :negative_squared_cross_mark: Faux en général. $70+u_0 < 7000+u_0$ pour tout $u_0\in\mathbb R$
        4. :white_check_mark: L'équation $70+u_0 = 2023$ a pour solution $u_0=1953$



!!! question "Question 6"
    $(u_n)$ est une suite arithmétique de nombres réels de premier terme $u_0=0$.
  
    Quelle est la phrase vraie dans la liste ?


    - [ ] Il est impossible d'avoir $u_{1000} = 1001$
    - [ ] Il est possible d'avoir $u_1 \times u_2 = -1$
    - [ ] Il est possible d'avoir $u_0 \times u_1 = +1$
    - [ ] Il est certain que $u_{1000} - u_{100} = u_{900}$

    ??? success "Réponse"
        La suite est arithmétique de premier terme $0$, donc $u_n=rn$ pour tout $n\in\mathbb N$, où $r$ est la raison.

        1. :negative_squared_cross_mark: C'est possible avec $r=1.001$
        2. :negative_squared_cross_mark: Faux, $u_1 \times u_2 = 2r^2 > 0$
        3. :negative_squared_cross_mark: Faux, $u_0 \times u_1 = 0$
        4. :white_check_mark: $u_{1000} - u_{100} = 1000r - 100r = 900r = u_{900}$


!!! question "Question 7"
    À quoi sert la fonction Python ci-dessous ?

    ```python
    def mystere(n):
        somme = 0
        for i in range(n):
            somme = somme + i**3
        return somme
    ```

    - [ ] Elle renvoie $\sum_{i=0}^{n-1} 3i$ en fonction de $n$
    - [ ] Elle renvoie $\sum_{i=0}^{n} 3i$ en fonction de $n$
    - [ ] Elle renvoie $\sum_{i=0}^{n} i^3$ en fonction de $n$
    - [ ] Elle renvoie $\sum_{i=0}^{n-1} i^3$ en fonction de $n$

    ??? success "Réponse"
        - `range(n)` fait $n$ tours de boucles de $0$ inclus à $n$ **exclu**.
        - `i**3` correspond à $i^3$

        La fonction renvoie donc $\displaystyle\sum_{i=0}^{n-1} i^3$ en fonction de $n$


!!! question "Question 8"
    $(u_n)$ est une suite géométrique de nombres réels de premier terme $u_0 = 3$ et de raison $2$.
    
    Quelle est la phrase vraie dans la liste ?

    - [ ] Il est possible d'avoir $u_n = 2022$ pour un certain $n$
    - [ ] On est certain que $u_n = u_{n+1} + 2$ pour tout $n$
    - [ ] Il est possible que $u_n = u_{n+1} + 2$ pour un certain $n$
    - [ ] On est certain que $u_{5} = 96$

    ??? success "Réponse"
        La suite est géométrique de premier terme $u_0 = 3$ et de raison $2$, donc $u_n=3×2^n$ et $u_{n+1} = 2u_n$ pour tout $n\in\mathbb N$.

        Les premières valeurs de $u_n$ sont : $3, 6, 12, 24, 48, 96, 192, 384, 768, 1536, 3072, \cdots$

        1. :negative_squared_cross_mark: La suite est croissante et évite $2022$ dans les premiers termes.
        2. :negative_squared_cross_mark: C'est faux pour $n=0$ par exemple.
        3. :negative_squared_cross_mark: C'est faux en général, la suite est croissante.
        4. :white_check_mark: $u_5 = 3×2^5 = 3×32 = 96$


!!! question "Question 9"
    $(u_n)$ est une suite géométrique de nombres réels de premier terme $u_0 = 1$.
    
    Quelle est la phrase vraie dans la liste ?

    - [ ] On est certain que $u_{2}$ est négatif
    - [ ] On est certain que $u_n$ et $u_{n+1}$ sont de même signe pour tout $n$
    - [ ] Il est possible que $u_2$ soit strictement négatif
    - [ ] On est certain que $u_0×u_2 = u_1^2$

    ??? success "Réponse"
        La suite est géométrique de premier terme $u_0 = 1$, donc $u_n=q^n$ pour tout $n\in\mathbb N$.

        1. :negative_squared_cross_mark: Faux avec $q=1$, $u_2 = 1 > 0$
        2. :negative_squared_cross_mark: Faux avec $q$ négatif
        3. :negative_squared_cross_mark: $u_2 = q^2 \geqslant 0$
        4. :white_check_mark: $u_0×u_2 = 1×q^2 = u_1^2$


!!! question "Question 10"
    On considère l'équation sur $\mathbb R$ : $3x^2-5x+7=0$

    - [ ] Cette équation possède 2 solutions.
    - [ ] Cette équation possède 3 solutions.
    - [ ] Cette équation possède 1 seule solution.
    - [ ] Cette équation n'a pas de solution.

    ??? success "Réponse"
        C'est un trinôme du second degré en $x$ de discriminant $\Delta = (-5)^2 - 4×3×7 = 25-84 <0$

        Ainsi, cette équation n'a pas de solution.


!!! question "Question 11"
    On considère l'équation sur $\mathbb R$ : $3x^2+bx-5=0$ où $b$ est un nombre réel inconnu.

    - [ ] Cette équation n'a pas de solution.
    - [ ] Cette équation possède 3 solutions.
    - [ ] Cette équation possède 1 seule solution.
    - [ ] Cette équation possède 2 solutions.

    ??? success "Réponse"
        C'est un trinôme du second degré en $x$ de discriminant $\Delta = b^2 - 4×3×(-5) = b^2+60 >0$

        Ainsi, cette équation possède 2 solutions.



!!! question "Question 12"
    On considère l'équation sur $\mathbb R$ : $ax^2+3x-5=0$ où $a$ est un nombre réel inconnu.
    
    Quelle phrase est vraie ?

    - [ ] Cette équation n'a pas de solution.
    - [ ] Cette équation possède 3 solutions.
    - [ ] Si $a$ est strictement positif, il est possible que l'équation aie une seule solution.
    - [ ] Si $a$ est négatif, il est possible que l'équation n'aie pas de solution.

    ??? success "Réponse"
        Si $a\neq0$, c'est un trinôme du second degré en $x$ de discriminant $\Delta = 3^2 - 4×a×(-5) = 9+20a$

        1. :negative_squared_cross_mark: Faux, avec $a=1$, par exemple.
        2. :negative_squared_cross_mark: Faux, toujours.
        3. :negative_squared_cross_mark: Faux, si $a>0$, alors $\Delta=9+20a>9>0$
        4. :white_check_mark: Vrai, avec $a=-1$ par exemple, $\Delta=9-20<0$



!!! question "Question 13"
    On considère l'équation sur $\mathbb R$ : $ax^4+bx^2+c=0$ où $a, b, c$ sont des nombres réels inconnus, avec $a\neq 0$ et $b^2-4ac=0$.
    
    Quelle phrase est vraie ?

    - [ ] Cette équation peut avoir $2$ ou $4$ solutions.
    - [ ] Cette équation possède toujours 2 solutions.
    - [ ] Cette équation n'a qu'une seule solution.
    - [ ] Cette équation peut avoir $0$, $1$ ou $2$ solutions.

    ??? success "Réponse"
        C'est un trinôme du second degré en $x^2$ de discriminant $\Delta = b^2 - 4×a×c = 0$, on peut donc l'écrire $(x^2 - b/a)^2$

        En fonction du signe de $b$ :

        - Si $b<0$, l'équation $ax^4+bx^2+c=0$ n'a aucune solution.
        - Si $b=0$, l'équation $ax^4+bx^2+c=0$ a une seule solution.
        - Si $b>0$, l'équation $ax^4+bx^2+c=0$ a deux solutions.


!!! question "Question 14"
    Quelle phrase est vraie ?


    - [ ] Un angle en radians est toujours compris entre $-\pi$ et $\pi$.
    - [ ] Un angle en radians est toujours compris entre $0$ et $\pi$.
    - [ ] Un angle en radians est toujours compris entre $0$ et $2\pi$.
    - [ ] Un angle en radians peut être tout nombre réel.

    ??? success "Réponse"
        Un angle en radians peut être tout nombre réel.




!!! question "Question 15"
    Quelle phrase est vraie ?

    - [ ] La tangente de $\frac{-\pi}4$ n'est pas définie.
    - [ ] La tangente de $\frac{-\pi}4$ est positif.
    - [ ] Le sinus de $\frac{-\pi}4$ est positif.
    - [ ] Le cosinus de $\frac{-\pi}4$ est positif.

    ??? success "Réponse"
        Le cosinus de $\frac{-\pi}4$ est positif.





!!! question "Question 16"
    Quel angle en radian a le même point image que $\frac{2023\pi}6$ sur le cercle trigonométrique ?

    - [ ] $\frac{\pi}6$
    - [ ] $\frac{-\pi}6$
    - [ ] $\frac{5\pi}6$
    - [ ] $\frac{-5\pi}6$

    ??? success "Réponse"
        $2023 = 12×168+7$, donc $\frac{2023\pi}6 = 168×2\pi + \frac{7\pi}6$
        
        Ainsi $\frac{2023\pi}6$, $\frac{7\pi}6$ et $\frac{-5\pi}6$ ont le même point image sur le cercle trigonométrique.



!!! question "Question 17"
    Que vaut $\tan\left(\frac\pi 6\right)$ ?

    - [ ] $1$
    - [ ] $\sqrt 3$
    - [ ] $\frac{1+\sqrt 3}2$
    - [ ] $\frac{1}{\sqrt 3}$

    ??? success "Réponse"
        On sait que $\sin\left(\frac\pi 6\right) = \dfrac{1}2$ et $\cos\left(\frac\pi 6\right) = \dfrac{\sqrt3}2$, d'où
        
        $\tan\left(\frac\pi 6\right) = \frac{1}{\sqrt 3}$




!!! question "Question 18"
    Que vaut $\tan\left(\frac\pi 4\right)$ ?

    - [ ] $\frac{1}{\sqrt 3}$
    - [ ] $\sqrt 3$
    - [ ] $\frac{1+\sqrt 3}2$
    - [ ] $1$

    ??? success "Réponse"
        On sait que $\sin\left(\frac\pi 4\right) = \dfrac{\sqrt 2}2$ et $\cos\left(\frac\pi 4\right) = \dfrac{\sqrt 2}2$, d'où

        $\tan\left(\frac\pi 6\right) = 1$


